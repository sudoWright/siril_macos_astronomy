# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# This file contains everything related to Siril.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced
# shellcheck disable=SC2034 # no exports desired

### dependencies ###############################################################

# Nothing here.

### variables ##################################################################

SIRIL_APP_DIR=$ARTIFACT_DIR/Siril.app
SIRIL_APP_CON_DIR=$SIRIL_APP_DIR/Contents
SIRIL_APP_RES_DIR=$SIRIL_APP_CON_DIR/Resources
SIRIL_APP_BIN_DIR=$SIRIL_APP_RES_DIR/bin
SIRIL_APP_ETC_DIR=$SIRIL_APP_RES_DIR/etc
SIRIL_APP_LIB_DIR=$SIRIL_APP_RES_DIR/lib
SIRIL_APP_FRA_DIR=$SIRIL_APP_CON_DIR/Frameworks
SIRIL_APP_PLIST=$SIRIL_APP_CON_DIR/Info.plist

SIRIL_BUILD=${SIRIL_BUILD:-0}

SIRIL_URL=https://gitlab.com/free-astro/siril.git
SIRIL_BRANCH=${SIRIL_BRANCH:-master}

if [ "$CI_PROJECT_NAME" = "siril" ]; then   # running Siril CI
  SIRIL_SRC_DIR=$CI_PROJECT_DIR
else                                    # not running Siril CI
  # Use default directory if not provided.
  if [ -z "$SIRIL_SRC_DIR" ]; then
    SIRIL_SRC_DIR=$SRC_DIR/siril-$SIRIL_BRANCH
  fi
fi

SIRIL_BLD_DIR=$BLD_DIR/$(basename "$SIRIL_SRC_DIR")  # we build out-of-tree

### functions ##################################################################

function siril_get_version_from_moduleset
{
  xmllint \
    --xpath "string(//moduleset/meson[@id='siril']/branch/@tag)" \
    "$SELF_DIR"/modulesets/siril.modules
}

function siril_get_version_from_config_h
{
  grep "#define VERSION" "$SIRIL_BLD_DIR"/src/config.h |
    awk -F '"' '{ print $2 }'
}

### main #######################################################################

# Nothing here.
